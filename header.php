<?php

/**

 * The template for displaying the header

 *

 * Displays all of the head element and everything up until the "container" div.

 *

 * @package WordPress

 * @subpackage FoundationPress

 * @since FoundationPress 1.0.0

 */



?>

<html class="no-js" <?php language_attributes(); ?> >

	<head>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Favicon -->

		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/tot-favicon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">

		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Nixie+One|Work+Sans:400,500,600,700' rel='stylesheet' type='text/css'>

		<?php wp_head(); ?>

		<script>

		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



		  ga('create', 'UA-71683814-2', 'auto');

		  ga('send', 'pageview');



		</script>

	</head>


	<body <?php body_class(); ?>>



	<?php if( is_page('home')) { ?>



		<div class="purp-overlay show-for-medium"></div>

		<video autoplay loop muted width="100%" class="full-bg-video show-for-medium">

			<source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/files/TOT-back-1.6.mp4" type="video/mp4">

		</video>



		<div class="loader show-for-medium">

	 		<video autoplay width="100%" id="loader" poster="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/landing-white-poster.jpg" class="animated fadeIn">

				<source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/files/tot-landing-clip.mp4" type="video/mp4">

			</video>

		</div>



	<?php } ?>



	<?php do_action( 'foundationpress_after_body' ); ?>









	<section class="container">

		<?php do_action( 'foundationpress_after_header' ); ?>

			<?php if( ! is_page('home')) { ?>

				<div class="sub-head">

					<div class="large-4 columns small-head">

						<a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/tot-logo-sub.png"></a>

					</div>

					<div class="large-8 columns">

						<h2><?php the_title(); ?></h2>

						<div class="short-divider show-for-small-only"></div>

					</div>

				</div>

			<?php } ?>

