<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

	
	
	
		<div id="footer-container">
			<?php get_template_part( 'parts/nav' ); ?>
			<footer id="footer">
				<div class="row fullWidth collapse">
					<div class="large-6 columns submit">
						<a data-open="trivia-modal">Submit a trivia question</a>


					</div>
					<div class="large-4 columns">
						<ul class="inline-block social-links">
							<li><a href="https://www.facebook.com/moxyjobs/?fref=ts" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-round-white-FB.png" alt="icon"></a></li>
							<li><a href="https://www.linkedin.com/company/2323375?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A1125916951450475385879%2CVSRPtargetId%3A2323375%2CVSRPcmpt%3Aprimary" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-round-white-LI.png" alt="icon"></a></li>
							<li><a href="https://twitter.com/digmoxy" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-round-white-TW.png" alt="icon"></a></li>
						</ul>
					</div>
					<div class="large-1 columns">
						<div class="diagonal-line show-for-large"></div>
					</div>
					<div class="large-1 columns moxy-text">
						<a href="http://digmoxy.com" target="_blank">Moxy</a>
					</div>
				</div>
			</footer>
		</div>
	</section>
		<div class="xlarge reveal trivia-form" id="trivia-modal" data-reveal>
			<?php 
              $slug = get_page_by_path('submit-a-trivia-question',OBJECT,'page');
              $post = get_post($slug);
              $title = apply_filters('the_title', $post->post_title);
              $content = apply_filters('the_content', $post->post_content); 
            ?>
        
            <?php echo $content; ?>

		</div>

<?php do_action( 'foundationpress_layout_end' ); ?>

<?php wp_footer(); ?>

<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
