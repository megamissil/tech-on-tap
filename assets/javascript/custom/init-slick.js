$(document).ready(function() {

  	$('.gallery-slider').slick({
    	arrows: true,
    	dots: false
 	});

 	  $('.slider-nav').slick({
	  	slidesToShow: 4,
	  	slidesToScroll: 1,
	  	asNavFor: '.gallery-slider',
	  	centerMode: true,
	  	focusOnSelect: true
	});

});