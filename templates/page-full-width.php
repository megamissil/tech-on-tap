<?php
/*
Template Name: Full Width
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>

<div id="page-full-width" role="main">

  <?php if(is_page('when-where')) { ?>
      <section class="when-section section">
        <div class="content">
          <div class="row">
            <div class="small-12 columns">
                <?php 
                  $slug = get_page_by_path('when-where',OBJECT,'page');
                  $post = get_post($slug);
                  $title = apply_filters('the_title', $post->post_title);
                  $content = apply_filters('the_content', $post->post_content); 
                ?>
            
                <?php echo $content; ?>

            </div>
          </div>
        </div>
        <?php get_footer(); ?>
      </section>
      
  <?php } elseif(is_page('contact-us')) { ?>
    <section class="contact-section section">
      <div class="content">
        <div class="row">
          <div class="small-12 columns">
              <?php 
                $slug = get_page_by_path('contact-us',OBJECT,'page');
                $post = get_post($slug);
                $title = apply_filters('the_title', $post->post_title);
                $content = apply_filters('the_content', $post->post_content); 
              ?>
          
              <?php echo $content; ?>

          </div>
        </div>
      </div>
      <?php get_footer(); ?>
    </section>

  <?php } elseif(is_page('sponsors')) { ?>
    <section class="sponsor-section section">
      <div class="content">
        <div class="row">
          <div class="small-12 columns">
              <?php 
                $slug = get_page_by_path('sponsors',OBJECT,'page');
                $post = get_post($slug);
                $title = apply_filters('the_title', $post->post_title);
                $content = apply_filters('the_content', $post->post_content); 
              ?>

              <?php echo $content; ?>

                <div class="row small-up-1 medium-up-2 large-up-4 sponsor-list">
                  <div class="column">
                    <a href="http://digmoxy.com" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sponsors-moxy.png" alt="sponsor"></a>
                    <div class="diagonal-line show-for-large"></div>
                  </div>
                  <div class="column">
                    <a href="http://avondalebrewing.com" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sponsors-avondale.png" alt="sponsor"></a>
                    <div class="diagonal-line show-for-large"></div>
                  </div>
                  <div class="column">
                    <a href="http://platypi.io" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sponsors-platypi.png" alt="sponsor"></a>
                    <div class="diagonal-line show-for-large"></div>
                  </div>
                  <div class="column">
                    <a href="https://www.facebook.com/COSMOSPIZZA/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sponsors-cosmos.png" alt="sponsor"></a>
                  </div>
                </div>

          </div>
        </div>
        
      </div>
      <?php get_footer(); ?>
    </section>
  <?php } ?>
</div>
