<?php
/*
Template Name: Gallery
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>

<div id="page-full-width" role="main">

  <section class="gallery-section section">
    <div class="content">
      <div class="row">
        <div class="large-12 columns">
            <?php 
              $slug = get_page_by_path('gallery',OBJECT,'page');
              $post = get_post($slug);
              $title = apply_filters('the_title', $post->post_title);
              $content = apply_filters('the_content', $post->post_content); 
            ?>
        
            <?php echo $content; ?>
          </div>
       </div>
    </div>
    <?php get_footer(); ?>
  </section>
</div>

