<?php
/**
 * Template part for navigation
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<?php if(is_page('home')) { ?>
	<ul class="nav">
		<li><a href="/when-where/">When & Where</a></li>
		<li><a href="/gallery/">Gallery</a></li>
		<li><a href="/sponsors/">Sponsors</a></li>
		<li><a href="/contact-us/">Contact Us</a></li>
	</ul>
<?php } else { ?>
	<ul class="nav">
		<li><a href="/home/">Return to Main</a></li>
		<li><a href="/gallery/">Gallery</a></li>
		<li><a href="/sponsors/">Sponsors</a></li>
		<li><a href="/contact-us/">Contact Us</a></li>
	</ul>
<?php } ?>