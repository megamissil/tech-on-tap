<?php
/*
Template Name: Splash
*/
//get_header(); ?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<!-- Favicon -->
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/tot-favicon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Paytone+One|Source+Sans+Pro:400,600' rel='stylesheet' type='text/css'>
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>
	<div class="row">
		<div class="large-12 columns splash-bg">
			<div class="logo-container text-center">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/tot-logo-white-shadow.png" alt="logo">
			</div>

			<div class="splash-content text-center">
				<h1>New site coming soon!</h1>

				<div class="row">
					<div class="medium-8 medium-centered columns text-center">
						<div class="long-divider"></div>
						<div class="short-divider"></div>
						
						<p>Keep checking back here for more details regarding<br class="show-for-large-up"> Tech on Tap, and how you can get involved with<br br class="show-for-large-up"> Birmingham's growing tech community!</p>

						<div class="shortest-divider"></div>

						<p>In the meantime, connect with us on:</p>

						<ul class="icons">
							<li><a href="https://www.facebook.com/moxyjobs/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-F.png" alt="fb"></a></li>
							<li><a href="https://twitter.com/digmoxy" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-T.png" alt="tw"></a></li>
							<li><a href="https://www.linkedin.com/company/2323375?trk=vsrp_companies_cluster_name&trkInfo=VSRPsearchId%3A1125916951448312367827%2CVSRPtargetId%3A2323375%2CVSRPcmpt%3Acompanies_cluster" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-L.png" alt="in"></a></li>
						</ul>
						<p><a href="http://digmoxy.com" class="moxy-button" target="_blank">Moxy</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>