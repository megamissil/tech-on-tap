
<?php
/*
Template Name: Home
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>


<section class="home-section">
	<!-- <div class="purp-overlay show-for-medium"></div>
	<video autoplay loop muted width="100%" class="full-bg-video show-for-medium">
		<source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/files/tot-home-clip-short.mp4" type="video/mp4">
	</video> -->
	<div class="home-content content">
		<div class="row">
			<div class="small-12 columns">
<!-- 				<h5 class="show-for-large">Welcome To</h5>
				<div class="long-divider show-for-large"></div>
				<div class="short-divider show-for-large"></div> -->
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/tot-logo-main.png">
				<p>Where Birmingham's Finest</p>
				<h3>Designers</h3>
				<h3>Developers</h3>
				<p class="home-and">And</p>
				<h3>Tech Specialists</h3>
				<p>Meet for a beer and some friendly competition.</p>

				<?php//get_template_part( 'parts/nav' ); ?>
			</div>
		</div>
	</div>
	<?php get_footer(); ?>
</section>
<?php // get_footer(); ?>